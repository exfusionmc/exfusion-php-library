<?php
namespace ExfusionAPIExamples; //This namespace can be changed or removed.
use Exfusion\API;
include('library/ExfusionAPI.inc.php');
$playerAPI = new \Exfusion\API\PlayerAPI('MY_API_TOKEN'); //Replace this with your Exfusion API token.
$response  = $playerAPI->getData('UUID'); //Replace this with the UUID of the player you'd like to retrieve data for.
if ($response instanceof API\Error) {
    $response->displayMessage(); //Don't want an error to display? Comment this line out.
} //$response instanceof API\Error
else {
    if (is_array($response)) {
        echo 'I, ' . $response["name"] . ' am a ' . $response["rank"] . ' on the Exfusion Minecraft server.';
    } //is_array($response)
}
?>