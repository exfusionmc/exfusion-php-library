<?php
namespace ExfusionAPIExamples; //This namespace can be changed or removed.
use Exfusion\API;
include('library/ExfusionAPI.inc.php');
$friendsAPI       = new \Exfusion\API\FriendsAPI('MY_API_TOKEN'); //Replace this with your Exfusion API token.
$playerAPI        = new \Exfusion\API\PlayerAPI('MY_API_TOKEN'); //Replace this with your Exfusion API token.
$friends_response = $friendsAPI->getData('c08c8d55-805d-4cd6-bcab-7eeb1637f31c'); //Replace this with the UUID of the player you'd like to retrieve data for.
if ($friends_response instanceof API\Error) {
    $friends_response->displayMessage();
} //$response instanceof API\Error
else {
    if (is_array($friends_response)) {
        echo '<b>I am friends with ' . sizeof($friends_response["friends"]) . ' players on the Exfusion server.</b> Their names and ranks are:';
        echo '<ul>';
        foreach ($friends_response["friends"] as $friend) {
            $friends_playerdata                  = array();
            $friends_playerdata[$friend["uuid"]] = $playerAPI->getData($friend["uuid"]);
            if ($friends_playerdata[$friend["uuid"]] instanceof API\Error) {
                $friends_playerdata[$friend["uuid"]]->displayMessage();
            } //$friends_playerdata[$friend["uuid"]] instanceof API\Error
            else {
                if (is_array($friends_playerdata[$friend["uuid"]])) {
                    echo '<li>' . $friends_playerdata[$friend["uuid"]]["name"] . ' (' . $friends_playerdata[$friend["uuid"]]["rank"] . ')</li>';
                } //is_array($friends_playerdata[$friend["uuid"]])
            }
        } //$friends_response["friends"] as $friend
        echo '</ul>';
    } //is_array($response)
}
?>