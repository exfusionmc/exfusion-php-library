## **Exfusion PHP Library** ##
---
### **Prerequisites** ###
- Web server running PHP 5.4 or upwards.
- cURL extension on your web server.
- An API token (get one by typing /api on the Minecraft server).

### **Getting Started** ###
Our library offers a plethora of APIs that are used for retrieving different types of data. The different APIs found within this library are:

**Player API:** Allows you to retrieve advanced information about a UUID, including their rank, amount of tokens, last join, currently joined server and game statistics

**Friends API:** Returns the UUIDs of a person's friends and an epoch-formatted time describing when the two players became friends. (Note that unless the user explicitly enables their friends data to be accessed via an API, you will not be able to use this for them).

There's also more APIs coming soon, including a server API to monitor the general status of the server.
Once you've decided your starting point, accustom yourself to our library by referring to the examples below.

### **Code Samples** ###

**Player API Example**
```
#!php
<?php
namespace ExfusionAPIExamples; //This namespace can be changed to whatever you desire, or even totally removed.
use Exfusion\API;
include('library/ExfusionAPI.inc.php');
$playerAPI = new \Exfusion\API\PlayerAPI('MY_API_TOKEN'); //Replace this with your Exfusion API token.
$response  = $playerAPI->getData('UUID'); //Replace this with the UUID of the player you'd like to retrieve data for.
if ($response instanceof API\Error) {
    $response->displayMessage(); //Don't want an error to display? Comment this line out.
} //$response instanceof API\Error
else {
    if (is_array($response)) {
        echo 'I, '.$response["name"].' am a '.$response["rank"].' on the Exfusion Minecraft server.';
    } //is_array($response)
}
?>
```
**Friends API Example**
```
#!php
<?php
namespace ExfusionAPIExamples; //This namespace can be changed or removed.
use Exfusion\API;
include('library/ExfusionAPI.inc.php');
$friendsAPI = new \Exfusion\API\FriendsAPI('MY_API_TOKEN');
$response  = $friendsAPI->getData('c08c8d55-805d-4cd6-bcab-7eeb1637f31c');
if ($response instanceof API\Error) {
    $response->displayMessage();
} //$response instanceof API\Error
else {
    if (is_array($response)) {
    	echo '<b>I am friends with '.sizeof($response["friends"]).' players on the Exfusion server.</b> Their UUIDs are:';
    	echo '<ul>';
        foreach($response["friends"] as $friend){
        	echo '<li>'.$friend["uuid"].'</li>';
        }
        echo '</ul>';
    } //is_array($response)
}
?>
```
The friends API only returns the uuid of the friend and the time at which they became friends, in epoch value.

If you want to show more information about your friends, we can hook into the player API as shown on this next example.
```
#!php
<?php
namespace ExfusionAPIExamples; //This namespace can be changed or removed.
use Exfusion\API;
include('library/ExfusionAPI.inc.php');
$friendsAPI       = new \Exfusion\API\FriendsAPI('MY_API_TOKEN'); //Replace this with your Exfusion API token.
$playerAPI        = new \Exfusion\API\PlayerAPI('MY_API_TOKEN'); //Replace this with your Exfusion API token.
$friends_response = $friendsAPI->getData('c08c8d55-805d-4cd6-bcab-7eeb1637f31c'); //Replace this with the UUID of the player you'd like to retrieve data for.
if ($friends_response instanceof API\Error) {
    $friends_response->displayMessage();
} //$response instanceof API\Error
else {
    if (is_array($friends_response)) {
        echo '<b>I am friends with ' . sizeof($friends_response["friends"]) . ' players on the Exfusion server.</b> Their names and ranks are:';
        echo '<ul>';
        foreach ($friends_response["friends"] as $friend) {
            $friends_playerdata                  = array();
            $friends_playerdata[$friend["uuid"]] = $playerAPI->getData($friend["uuid"]);
            if ($friends_playerdata[$friend["uuid"]] instanceof API\Error) {
                $friends_playerdata[$friend["uuid"]]->displayMessage();
            } //$friends_playerdata[$friend["uuid"]] instanceof API\Error
            else {
                if (is_array($friends_playerdata[$friend["uuid"]])) {
                    echo '<li>' . $friends_playerdata[$friend["uuid"]]["name"] . ' (' . $friends_playerdata[$friend["uuid"]]["rank"] . ')</li>';
                } //is_array($friends_playerdata[$friend["uuid"]])
            }
        } //$friends_response["friends"] as $friend
        echo '</ul>';
    } //is_array($response)
}
?>
```

If you require further help, please visit the Exfusion Developer Hub at www.exfusion.net/developers - don't contact us directly because we won't assist with development using our API. It is provided on an as-is basis.