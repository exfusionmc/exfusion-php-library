<?php
namespace ExfusionAPIExamples; //This namespace can be changed or removed.
use Exfusion\API;
include('library/ExfusionAPI.inc.php');
$friendsAPI = new \Exfusion\API\FriendsAPI('MY_API_TOKEN'); //Replace this with your Exfusion API token.
$response   = $friendsAPI->getData('c08c8d55-805d-4cd6-bcab-7eeb1637f31c'); //Replace this with the UUID of the player you'd like to retrieve data for.
if ($response instanceof API\Error) {
    $response->displayMessage();
} //$response instanceof API\Error
else {
    if (is_array($response)) {
        // Start of your code to manipulate the data (this is an example so we've included some example code!)
        echo '<b>I am friends with ' . sizeof($response["friends"]) . ' players on the Exfusion server.</b> Their UUIDs are:';
        echo '<ul>';
        foreach ($response["friends"] as $friend) {
            echo '<li>' . $friend["uuid"] . '</li>';
        } //$response["friends"] as $friend
        echo '</ul>';
        // End of your code to manipulate the data
    } //is_array($response)
}
?>