<?php
/*
 * Copyright (C) Exfusion 2016 - All rights reserved
 * Licensed for use on any site with attribution and clearly visible link to exfusion.net
 * Lead developer: Edan Brooke (edanbrooke.com)
 * Date: 16/08/2016
 * Time: 14:49
 * > It is prohibited to copy or claim as your own any of the resources within this API or on our website.
 * > You may not download a copy of our website except by viewing the website on your device as a regular
 * > user would do. Fair usage policy applies. All rights reserved.
 * > Our API is provided as-is without any warranties and we do not endorse applications created with this API.
 */
namespace ExfusionAPIExamples;
set_include_path(realpath(dirname(__FILE__)) . '/');
spl_autoload_extensions('.class.php');
spl_autoload_register(function($class) {
    include get_include_path() . str_replace('\\', '/', $class) . '.class.php';
});