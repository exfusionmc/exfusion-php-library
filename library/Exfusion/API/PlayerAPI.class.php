<?php
/*
 * Copyright (C) Exfusion 2016 - All rights reserved
 * Licensed for use on any site with attribution and clearly visible link to exfusion.net
 * Lead developer: Edan Brooke (edanbrooke.com)
 * Date: 16/08/2016
 * Time: 21:20
 * > It is prohibited to copy or claim as your own any of the resources within this API or on our website.
 * > You may not download a copy of our website except by viewing the website on your device as a regular
 * > user would do. Fair usage policy applies. All rights reserved.
 * > Our API is provided as-is without any warranties and we do not endorse applications created with this API.
 */
namespace Exfusion\API;
class PlayerAPI extends ExfusionAPI {
    protected $uri = 'https://www.exfusion.net/api/player';
    protected $api_key;
    public function __construct($api_key) {
        $this->api_key = $api_key;
    }
    public function getData($uuid) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->uri . '?uuid=' . $uuid);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'X-Exfusion-Token: ' . $this->api_key
        ));
        $data = curl_exec($ch);
        if (curl_errno($ch)) {
            return new Error("Curl: " . curl_error($ch));
        } //curl_errno($ch)
        if (curl_errno($ch)) {
            return new Error("Curl: " . curl_error($ch));
        } //curl_errno($ch)
        $data = json_decode($data, true);
        if (is_array($data)) {
            if (isset($data["error"])) {
                return new Error($data["error"]);
            } //isset($data["error"])
            return $data;
        } //is_array($data)
        return new Error("Malformed response - remote API could be undergoing maintenance");
    }
}