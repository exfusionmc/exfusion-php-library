<?php
/*
 * Copyright (C) Exfusion 2016 - All rights reserved
 * Licensed for use on any site with attribution and clearly visible link to exfusion.net
 * Lead developer: Edan Brooke (edanbrooke.com)
 * Date: 16/08/2016
 * Time: 21:20
 * > It is prohibited to copy or claim as your own any of the resources within this API or on our website.
 * > You may not download a copy of our website except by viewing the website on your device as a regular
 * > user would do. Fair usage policy applies. All rights reserved.
 * > Our API is provided as-is without any warranties and we do not endorse applications created with this API.
 */
namespace Exfusion\API;
class Error {
    private $message;
    public function __construct($message = null) {
        $this->message = $message;
    }
    public function getMessage() {
        return $this->message;
    }
    public function setMessage($message) {
        $this->message = $message;
    }
    public function displayMessage() {
        echo 'An error occurred when trying to implement the Exfusion API to retrieve some data.';
        if (!is_null($this->message)) {
            echo 'The error was: <b>' . $this->message . '</b>.';
        } //!is_null($this->message)
    }
}